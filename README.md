# README

## Beskrivelse:
Denne guide viser hvordan en golden software grd elevationsfil kan laves til Aarhus Workbench ved brug af opensource og freeware programmer.
Scriptet er udviklet af Simon Makwarth. Ved spørgsmål kontakt Simon Makwarth på simak@mst.dk

## Nødvendige programmer:
- python3
- OSGeo4W shell, python scriptet anvender per default batch-filen ”F:\GKO\fag\faggruppe\geofysik\Til_Egenproduktion_geofysik\Lav_DEM_filer\OSGeo4W64\OSGeo4W.bat”  

## Vejledning:
Læs "Guide til at Lave surfer DEM fil.docx"
