::===============================================================
:: This script unzip DEM files Dee script viser hvordan en golden software grd elevationsfil 
:: kan laves til Aarhus Workbench ved brug af opensource og freeware programmer.
:: Ved sprgsml kontakt Simon Makwarth p simak at mst dot dk
::===============================================================


@ECHO OFF
setlocal enabledelayedexpansion


REM SET osgeo_path="C:\OSGeo4W64\OSGeo4W.bat"
SET osgeo_path="F:\GKO\fag\faggruppe\geofysik\Til_Egenproduktion_geofysik\Lav_DEM_filer\OSGeo4W64\OSGeo4W.bat"
REM SET sevenzip_path="C:\Program Files\7-Zip\7z.exe"
SET sevenzip_path="F:\GKO\fag\faggruppe\geofysik\Til_Egenproduktion_geofysik\Lav_DEM_filer\7-Zip\7z.exe"

REM Unzipping all zip-files within the same folder as the bat file
echo ############ Initiating unzip ############
SET input_zip=zip
echo %sevenzip_path%
for %%f in (%~dp0*.%input_zip%) do (
  echo INPUT DIR: %%f
  echo OUTPUT DIR: %~dp0ZIP
  %sevenzip_path% x -y -o"%~dp0\ZIP" "%%~f"
)
echo ############ Done unzipping ############



REM Translates ESRI ascii files to golden software grd files 
echo ############ Initiating tranlation ############
SET input_trans=asc
SET output_trans=grd

if not exist "%~dp0GRD\" mkdir %~dp0GRD\

for %%g in (%~dp0ZIP\*.%input_trans%) do (
  echo INPUT DIR: %%g
  echo OUTPUT DIR: %~dp0GRD\%%~ng.%output_trans%
  call %osgeo_path% gdal_translate -a_srs EPSG:25832 -a_nodata -9999 -of GS7BG %%g %~dp0GRD\%%~ng.%output_trans%
  REM %osgeo_path%  gdal_translate -a_srs EPSG:25832 -a_nodata -9999 -of GSAG %%g %~dp0GRD\%%~nf.%output_trans%
)

echo ############ Done tranlation ############



REM Merge all grd files into one single GRD file
echo ############ Initiating merge ############

SET input_merge=grd
SET output_merge=grd

for %%f in (%~dp0GRD\*.%input_merge%) do (
  SET var=!var!%%f  
)
echo INPUT DIR: %var%
echo OUTPUT DIR: %~dp0\DEM_file.%output_merge%

call %osgeo_path% gdal_merge -n -9999 -a_nodata -9999 -of GS7BG -o %~dp0\DEM_file.%output_merge% %var%

echo ############ Done merge ############


REM removing the GRD and ZIP folder
echo ############ Initiating deleting unused files ############
RMDIR /s /q %~dp0GRD\
RMDIR /s /q %~dp0ZIP\
echo ############ Done deleting unused files ############
pause

 