from zipfile import ZipFile
import os
import glob
import concurrent.futures

__author__ = 'Simon Makwarth <simak@mst.dk>'
__name__ = 'make_grd_dem_file'
__licence__ = 'GNU GPLv3'
__descr__ = f'''
    {__name__} inputs folder of zipfiles containing DEM geotiff files. This script does the following:
        1) unzips all zipfiles (new folder ZIP).
        2) Renders all geotiff files in desired resolution e.g. 5x5 meters (new folder RESIZE).
            NB: This action is done by multitrhreading, which will mix the output from the several osgeo prompts
        3) Merges all newly rendered geotiff files into a single geotiff (new folder MERGE).
            NB: osgeo python throws an error when the osgeo python file gdal_merge.py is completed.
        4) translate the merged geotiff file to surfer grid, golden software grd (root folder).
'''


class MergeGrid:

    def __init__(self, root_folder, gdal_path, xres=10, yres=10):
        """
        Descr:  This class is translating geotiff into Golden software grid file (surfer).
                The script generates cmd commands used for the terminal OSGeo4W.bat.
        :param root_folder: the root folder of the zip-files containing grid files
        :param gdal_path: the path for the OSGeo4W.bat
        :param xres: resolution in meters (srid:25832) of the resized tiff file in the x-direction
        :param yres: resolution in meters (srid:25832) of the resized tiff file in the y-direction
        """
        self.gdal_path = gdal_path
        self.root_folder = root_folder.replace('/', '\\')
        self.zip_dst = f'{self.root_folder}/ZIP'
        self.resize_dst = f'{self.root_folder}/RESIZE'
        self.merge_dst = f'{self.root_folder}/MERGE'
        self.translate_dst = f'{self.root_folder}'
        self.xres = xres
        self.yres = yres

    def unzip_grids(self):
        """
        Descr:  This function unzips all zipfile containing geotifs into the same subfolder
        """
        zip_src = glob.glob(f'{self.root_folder}/*.zip')

        # make subfolder ZIP if not exists
        if not os.path.exists(self.zip_dst):
            os.makedirs(self.zip_dst)

        # unzip all zip files at src path and extract into subfolder
        for zip_file in zip_src:
            with ZipFile(zip_file, 'r') as zipObj:
                print(f'\n\nUnzipping: {zip_file}')
                zipObj.extractall(self.zip_dst)

    def resize_grid(self, tiff_file):
        """
        Descr:  This function outputs a tiff file with a fixed resolution/descretisation of the original geotif files,
                default output resolution is 10x10 meters at srid 25832
        :param tiff_file: geotiff to be resized
        """
        # generate command for each tiff file and execute via osgeo4w batch file
        resize_command = f'call {self.gdal_path} gdalwarp '
        resize_command += f'-s_srs EPSG:25832 -of GTiff -tr {self.xres} {self.yres} -r average'

        pathname, extension = os.path.splitext(tiff_file)
        filename = pathname.split('\\')[-1]
        resize_command_full = f'{resize_command} {tiff_file} {self.resize_dst}/{filename}.tif'
        print(f'\n\nResizing: {tiff_file}')
        os.system(resize_command_full)

    def resize_grids_batch(self, max_workers=50):
        """
        Descr:  Multithreading of the resize_grid function
        :param max_workers: maximum number of workers (threads) for multithreading
        :return:
        """
        resize_src = glob.glob(f'{self.zip_dst}/*.tif')

        # make subfolder RESIZE if not exists
        if not os.path.exists(self.resize_dst):
            os.makedirs(self.resize_dst)

        with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
            executor.map(self.resize_grid, resize_src)

    def merge_grids(self):
        """
        Descr:  This function merges the geotif from the source path "merge_src" into
                into a single geotif file.
        NB: The osgeo command uses a text file as input grid, which is generated in this script.
        NB: Windows throws an error at the end of the merge which can be closed
        """
        merge_src = glob.glob(f'{self.resize_dst}/*.tif')

        # make subfolder MERGE if not exists
        if not os.path.exists(self.merge_dst):
            os.makedirs(self.merge_dst)

        # make an optfile containing the path of all grid to be merged
        optfile_path = f'{self.merge_dst}/optfile.txt'
        with open(optfile_path, 'w') as optfile:
            for i, tiff in enumerate(merge_src):
                if i == 0:
                    optfile.write(tiff)
                else:
                    optfile.write(f'\n{tiff}')

        # generate command and execute via osgeo4w batch file
        merge_command = f'call {self.gdal_path} gdal_merge'
        merge_command += f'-a_nodata -9999 -of GTiff --optfile {optfile_path} -o {self.merge_dst}/dem_merge.tif'
        print(f'\n\nMerging grid files into {self.merge_dst}/dem_merge.tif')
        print(merge_command)
        os.system(merge_command)

    def translate_grids(self):
        """
        Descr:  This function translates the geotif file into golden software grd grid (surfer).
        Note: the loop in this function should not be necessary since input is expected to be a single tif file
        """
        translate_src = glob.glob(f'{self.merge_dst}/*.tif')

        # make root folder if not exists
        if not os.path.exists(self.translate_dst):
            os.makedirs(self.translate_dst)

        # generate command and execute via osgeo4w batch file
        translate_command = f'call {self.gdal_path} gdal_translate -a_srs EPSG:25832 -a_nodata -9999 -of GS7BG'
        for tiff_file in translate_src:
            filename = 'DEM_file'
            translate_command_full = f'{translate_command} {tiff_file} {self.translate_dst}/{filename}.grd'
            print(f'\n\nTranslating: {tiff_file}')
            os.system(translate_command_full)


# EDIT HERE: path for osgeo batch file, consider using a local version of osgeo4w
osgeo_path = r'F:\GKO\fag\Faggruppe\geofysik\Til_Egenproduktion_geofysik\make_grd_dem_file\OSGeo4W64\OSGeo4W.bat'

# EDIT HERE: root path for the DEM zipfiles
root_folder = r'C:\gitlab\her'  # no whitespaces in path

# EDIT HERE: new resolution in meters since projection srid is 25832
xres = 6
yres = xres

mg = MergeGrid(root_folder=root_folder, gdal_path=osgeo_path, xres=xres, yres=yres)  # initiates the class
# each function is executed separately, uncomment one or more functions if the action is not needed
mg.unzip_grids()  # unzips the zip files, located in subfolder ZIP
mg.resize_grids_batch()  # resizing the grid files into 10x10 meter, located in subfolder RESIZE
mg.merge_grids()  # merges the grid files into a single file, located in subfolder MERGE
mg.translate_grids()  # translates the grid file into surfer grid, located in root folder
